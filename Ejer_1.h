using namespace std;
#include <stdlib.h>
#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>
#include <ctime>
#include <chrono>
#include <math.h>
#include "Nodo.h"
#include "Lista.h"

class Ejercicio {	
	
	public:
        
        void llena(Lista *&arr){
			Lista *lis = new Lista();
			arr = lis;
		}
        
        //la funcion de tipo bool validar_int recibe un string de entrada por terminal
		bool validar_int(string in){
			//para validar que la entrada sea un int
			bool is_true = true;
			//se crea is_true como una vaiable bool igual a true
			//y se ingresa en un ciclo for que dura mientras el string tenga nuevos caracteres
			for(int j = 0; j < in.size(); j++){
				//dentro del for se comparan los valores ASCII del ingreso con los valores 
				//que corresponden a los numeros de 0-9 y al caracter "-"
				if((in[j] < 48 || in[j] >57) && in[j] != 45){
				    //si cualquiera de los caracteres no es un numero valido o "-" la variable is_true se iguala a false 
					is_true = false;
				}
			}
			//la funcion retorna la variable is_true
			return is_true;
		}
		
		int Hash(int num){
			int ret;
			ret = (num % 19);
			return ret;
		}
		int Hash_p(int num){
			int ret;
			ret = (num % 19)+2;
			return ret;
		}
		
		
		int menu(){
			//esta funcion define un menú de opciones para el funcionamiento del programa
			//se definen un string in, para el ingreso de teclado, un bool y dos int
			string in;
			bool is_true;
			//is_true define si el ingreso del teclado es un numero valido
			//temp es la opcion que saldra del menu, y cond mantiene el menu corriendo en el caso que no se ingrese un valor adecuado
			int temp, cond = 0;
			while(cond == 0){
				//primero se imprimen las opciones del menu, dando al usuario control sobre que hacer luego
				cout<<"Agregar número [1]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Buscar número  [2]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Salir          [0]"<<endl;
				cout<<"------------------"<<endl;
				cout<<"------------------"<<endl;
				cout<<"Opción:  ";
				getline(cin, in);
				//in recibe un ingreso del teclado que se pasa a validar si puede ser un int valido
				is_true = this->validar_int(in);
				if(is_true == true){
					//si is_true es verdadero in se pasa a temp como un int
					temp = stoi(in);
					//y luego se comprueba que sea una opcion valida
					if(temp < 0 || temp > 2){
						//si el ingreso no se encuentra entre 0-2, se envia un mensaje de error
						//ya que no ha habido salida valida, cond no se modifica y se devuelve al inicio del ciclo
						cout<< "opción no valida, intente nuevamente"<<endl;
					}else{
						//si el ingreso es valido cond se iguala a 1, acabando el ciclo
						cond = 1;
					}
				}else{
					//si is_true es falso, se envia un mensaje de error
					//ya que no ha habido un ingreso valido, cond no se modifica y se devuelve al inicio del ciclo
					cout<<"Ingreso no válido, intente nuevamente"<<endl;
				}				
				
			}
			//se retorna temp, como un dato que indica que opcion decidio usar el usuario
			return temp;				
		}	
		
		void Prueba_lineal(int arr[], int num){
			int D, DX, N = 20;
			D = this->Hash(num);
			if(arr[D] != 0 && arr[D] == num){
				cout<<"La informacion se encuentra en la posicion ["<<D+1<<"]"<<endl;
			}else{
				DX = D +1;
				while(DX < N && arr[DX] != 0 && arr[DX] != num && DX != D){
					DX = DX+1;
					
					if(DX == N){
						DX = 1;
					}
				}
				if(arr[DX] == 0 || DX == D){
					cout<<"La informacion no se encuentra en el arreglo"<<endl;
				}else{
					cout<<"La informacion se encuentra en la posicion ["<< DX+1<<"]"<<endl;
				}
			}
		}
		
		void Prueba_cuadratica(int arr[], int num){
			int D, DX, I, N = 20;
			D = this->Hash(num);
			if(arr[D] != 0 && arr[D] == num){
				cout<<"La informacion se encuentra en la posicion ["<<D+1<<"]"<<endl;
			}else{
				I = 1;
				DX = D + (I*I);
				while(arr[DX] != 0 && arr[DX] != num){
					I = I+1;
					DX = D + (I*I);
					if(DX > N){
						I = 0;
						DX = 0;
						D = 0;
					}
				}
				if(arr[DX] == 0){
						cout<<"La informacion no se encuentra en el arreglo"<<endl;
				}else{
					cout<<"La informacion se encuentra en la posicion ["<< DX+1<<"]"<<endl;
				}
			}
		}
		
		void Doble_direccion(int arr[], int num){
			int D, DX, N = 20;
			D = this->Hash(num);
			if(arr[D] != 0 && arr[D] == num){
				cout<<"La informacion se encuentra en la posicion ["<<D+1<<"]"<<endl;
			}else{
				DX = this->Hash_p(D);
				while(DX < N && arr[DX] != 0 && arr[DX] != num && DX != D){
					DX = this->Hash_p(DX);
				}
				if(arr[DX] == 0 || arr[DX] != num){
						cout<<"La informacion no se encuentra en el arreglo"<<endl;
				}else{
					cout<<"La informacion se encuentra en la posicion ["<< DX+1<<"]"<<endl;
				}
			}
		}
					
		void Encadenamiento(Lista *arr[], int num){
			int D, N = 20;
			Nodo *Q;
			D = this->Hash(num);
			if(arr[D]->raiz != NULL && arr[D]->raiz->numero){
				cout<<"La informacion se encuentra en la posicion ["<<D<<"]"<<endl;
			}else{
				Q = arr[D]->raiz->sig;
				while(Q != NULL && Q->numero != num){
					Q = Q->sig;
				}
				if (Q == NULL){
					cout<<"La informacion no se encuentra en el arreglo"<<endl;
				}else{
					cout<<"La informacion se encuentra en la lista"<<endl;
				}
			}
		}
			
		
		
       
		
};

